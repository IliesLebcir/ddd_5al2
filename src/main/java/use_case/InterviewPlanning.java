package use_case;

import common.dto.CandidateDto;
import common.dto.RecruiterDto;
import common.request.ScheduleInterviewRequest;
import common.response.Response;
import interview_planning.Interview;

import java.util.List;

public class InterviewPlanning {

    public Response scheduleInterview(ScheduleInterviewRequest request, ICandidateRepository candidateRepository, IRecruiterRepository recruiterRepository) {
        Interview interview = new Interview();
        CandidateDto candidateDto = candidateRepository.getCandidateById(request.getCandidate());
        List<RecruiterDto> recruitersDto = recruiterRepository.getRecruiters();

        try {
            interview.scheduleInterview(candidateDto, recruitersDto, request.getDateTime());
            return new Response(interview);
        } catch (Error e) {
            return new Response(e);
        }
    }

}
