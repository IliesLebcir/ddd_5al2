package use_case;

import common.dto.CandidateDto;
import common.dto.RecruiterDto;

import java.rmi.server.UID;
import java.util.List;

public interface ICandidateRepository {

    CandidateDto getCandidateById(UID uid);


}
