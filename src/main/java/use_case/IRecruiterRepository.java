package use_case;

import common.dto.RecruiterDto;

import java.util.List;

public interface IRecruiterRepository {

    List<RecruiterDto> getRecruiters();


}
