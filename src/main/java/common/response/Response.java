package common.response;

public class Response<T> {
    T data;
    Error error;

    public Response(T data) {
        this.data = data;
    }

    public Response(Error error) {
        this.error = error;
    }

    public T getData() {
        return data;
    }

    public Error getError() {
        return error;
    }


}
