package common.request;

import java.rmi.server.UID;
import java.time.LocalDateTime;

public class ScheduleInterviewRequest {
    UID candidate;
    LocalDateTime dateTime;

    public ScheduleInterviewRequest(UID candidate, LocalDateTime dateTime) {
        this.candidate = candidate;
        this.dateTime = dateTime;
    }

    public UID getCandidate() {
        return candidate;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }
}
