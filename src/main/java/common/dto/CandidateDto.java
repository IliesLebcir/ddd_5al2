package common.dto;

import java.rmi.server.UID;

public class CandidateDto {

    UID uid;
    String lastName;
    String firstName;

    public CandidateDto(UID uid, String lastName, String firstName) {
        this.uid = uid;
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public int hashCode() {
        return this.uid.hashCode();
    }
}
