package common.dto;

import java.util.List;

public class RecruiterDto {

    String lastName;
    String firstName;
    List<SlotDto> slots;

    public RecruiterDto(String lastName, String firstName, List<SlotDto> slots) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.slots = slots;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public List<SlotDto> getSlots() {
        return slots;
    }

    public void setSlots(List<SlotDto> slots) {
        this.slots = slots;
    }
}
