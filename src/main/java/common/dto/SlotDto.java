package common.dto;

import java.time.LocalDate;
import java.time.LocalTime;

public class SlotDto {

    LocalDate date;
    LocalTime startingTime;
    LocalTime endingTime;

    public SlotDto(LocalDate date, LocalTime startingTime, LocalTime endingTime) {
        this.date = date;
        this.startingTime = startingTime;
        this.endingTime = endingTime;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getStartingTime() {
        return startingTime;
    }

    public void setStartingTime(LocalTime startingTime) {
        this.startingTime = startingTime;
    }

    public LocalTime getEndingTime() {
        return endingTime;
    }

    public void setEndingTime(LocalTime endingTime) {
        this.endingTime = endingTime;
    }
}
