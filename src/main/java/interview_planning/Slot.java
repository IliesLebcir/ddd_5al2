package interview_planning;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

public class Slot {

    final LocalDate date;
    final LocalTime startingTime;
    final LocalTime endingTime;

    public Slot(LocalDate date, LocalTime startingTime, LocalTime endingTime) {
        if (!checkHours(startingTime, endingTime) || !checkDate(date))
            throw new Error("Invalid dates and times");

        this.date = date;
        this.startingTime = startingTime;
        this.endingTime = endingTime;
    }

    private boolean checkHours(LocalTime startingTime, LocalTime endingTime) {
        return startingTime != null && endingTime != null && startingTime.isBefore(endingTime);
    }

    private boolean checkDate(LocalDate date) {
        return date != null && LocalDate.now().isBefore(date);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Slot slot = (Slot) o;
        return isOverlapping(slot);
    }

    private boolean isOverlapping(Slot slot) {
        return this.date.isEqual(slot.date)
                &&
                    (this.startingTime.isBefore(slot.endingTime) && this.startingTime.isAfter(slot.startingTime))
                        || (this.endingTime.isAfter(slot.startingTime) && this.endingTime.isBefore(slot.endingTime))
                        || (this.startingTime.equals(slot.startingTime) && this.endingTime.equals(slot.endingTime));

    }
}
