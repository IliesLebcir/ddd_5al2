package interview_planning;

import java.util.Objects;

class Candidate {

    final String lastName;
    final String firstName;

    public Candidate(String lastName, String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
    }

}
