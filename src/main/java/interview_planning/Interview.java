package interview_planning;

import common.dto.CandidateDto;
import common.dto.RecruiterDto;
import common.dto.SlotDto;

import java.rmi.server.UID;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Interview {

    private UID uid;
    private Slot slot;
    private Candidate candidate;
    private Recruiter recruiter;
    private InterviewStatus interviewStatus = InterviewStatus.WAITING;

    public void scheduleInterview(CandidateDto candidateDto, List<RecruiterDto> recruitersDto, LocalDateTime dateTime) {
        this.uid = new UID();
        this.candidate = new Candidate(candidateDto.getFirstName(), candidateDto.getLastName());
        this.slot = new Slot(dateTime.toLocalDate(), dateTime.toLocalTime(), dateTime.toLocalTime().plusHours(1));

        for (RecruiterDto recruiterDto : recruitersDto) {
            final List<Slot> slots = new ArrayList<>();
            for (SlotDto slotDto : recruiterDto.getSlots()) {
                slots.add(new Slot(slotDto.getDate(), slotDto.getStartingTime(), slotDto.getEndingTime()));
            }

            Recruiter recruiter = new Recruiter(recruiterDto.getFirstName(), recruiterDto.getLastName(), slots);

            if (recruiter.isAvailable(slot)) {
                this.recruiter = recruiter;
                break;
            }
        }

        if (this.recruiter == null) {
            throw new Error("Not found");
        }

        try {
            this.recruiter.scheduleSlot(slot);
            this.interviewStatus = InterviewStatus.SCHEDULED;
        } catch (Error e) {
            this.interviewStatus = InterviewStatus.CANCELED;
        }
    }

    public UID getUid() {
        return uid;
    }

    public Slot getSlot() {
        return slot;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public Recruiter getRecruiter() {
        return recruiter;
    }

    public InterviewStatus getInterviewStatus() {
        return interviewStatus;
    }
}
