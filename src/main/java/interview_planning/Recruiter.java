package interview_planning;

import java.rmi.server.UID;
import java.util.ArrayList;
import java.util.List;

class Recruiter {

    final UID id;
    final String lastName;
    final String firstName;
    final List<Slot> slots;

    Recruiter(String lastName, String firstName, List<Slot> slots) {
        this.id = new UID();
        this.lastName = lastName;
        this.firstName = firstName;
        this.slots = slots != null ? slots : new ArrayList<>();
    }

    void scheduleSlot(Slot slot) {
        if(isAvailable(slot) == false)
            throw new Error("Recruiter isn't available");
        this.slots.add(slot);
    }

    boolean isAvailable(Slot slot) {
        if (slot == null)
            throw new Error("Invalid slot");
        return this.slots.contains(slot) == false;
    }

}
