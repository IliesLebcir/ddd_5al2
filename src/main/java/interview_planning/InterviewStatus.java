package interview_planning;

public enum InterviewStatus {

    SCHEDULED,
    CANCELED,
    WAITING,
}
