package tests;

import common.dto.CandidateDto;
import common.dto.RecruiterDto;
import common.dto.SlotDto;
import common.request.ScheduleInterviewRequest;
import common.response.Response;
import interview_planning.Interview;
import org.junit.jupiter.api.Test;
import use_case.ICandidateRepository;
import use_case.IRecruiterRepository;
import use_case.InterviewPlanning;

import java.rmi.server.UID;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.fail;

class InterviewPlanningTest {

    @Test
    final void scheduleBasicInterviewSucceed() {
        ICandidateRepository candidateRepository = uid -> new CandidateDto(uid, "Sylvain", "Miron");
        IRecruiterRepository recruiterRepository = () -> new ArrayList<RecruiterDto>() {{
            add(new RecruiterDto("Julien", "Brouzes", new ArrayList<>()));
        }};
        ScheduleInterviewRequest scheduleInterviewRequest = new ScheduleInterviewRequest(new UID(), LocalDateTime.now().plusDays(1));
        Response response = (new InterviewPlanning()).scheduleInterview(scheduleInterviewRequest, candidateRepository, recruiterRepository);
        if (response.getData() == null)
            fail(response.getError());
    }

    @Test
    final void scheduleWithMultipleRecruiterInterviewSucceed() {
        ICandidateRepository candidateRepository = uid -> new CandidateDto(uid, "Sylvain", "Miron");
        IRecruiterRepository recruiterRepository = () -> new ArrayList<RecruiterDto>() {{
            add(new RecruiterDto("Julien", "Brouzes", new ArrayList<>()));
            add(new RecruiterDto("Ilies", "Lebcir", new ArrayList<>()));
        }};
        ScheduleInterviewRequest scheduleInterviewRequest = new ScheduleInterviewRequest(new UID(), LocalDateTime.now().plusDays(1));
        Response response = (new InterviewPlanning()).scheduleInterview(scheduleInterviewRequest, candidateRepository, recruiterRepository);
        if (response.getData() == null)
            fail(response.getError());
    }

    @Test
    final void scheduleWithOneSlotOccupiedInterviewSucceed() {
        LocalDateTime localDateTime = LocalDateTime.now().plusDays(1);
        ICandidateRepository candidateRepository = uid -> new CandidateDto(uid, "Sylvain", "Miron");
        IRecruiterRepository recruiterRepository = () -> new ArrayList<RecruiterDto>() {{
            add(
                    new RecruiterDto(
                            "Julien",
                            "Brouzes",
                            new ArrayList<SlotDto>() {{
                                add(new SlotDto(localDateTime.toLocalDate(), localDateTime.toLocalTime(), localDateTime.toLocalTime().plusHours(1)));
                            }}
                    )
            );
        }};
        ScheduleInterviewRequest scheduleInterviewRequest = new ScheduleInterviewRequest(new UID(), localDateTime);
        Response response = (new InterviewPlanning()).scheduleInterview(scheduleInterviewRequest, candidateRepository, recruiterRepository);
        if (response.getError() == null)
            fail("Should have failed");
    }

    @Test
    final void scheduleWithTwoSlotOccupiedInterviewSucceed() {
        LocalDateTime localDateTime1 = LocalDateTime.now().plusDays(1);
        LocalDateTime localDateTime2 = LocalDateTime.now().plusDays(2);
        LocalDateTime localDateTime3 = LocalDateTime.now().plusDays(3);
        ICandidateRepository candidateRepository = uid -> new CandidateDto(uid, "Sylvain", "Miron");
        IRecruiterRepository recruiterRepository = () -> new ArrayList<RecruiterDto>() {{
            add(
                    new RecruiterDto(
                            "Julien",
                            "Brouzes",
                            new ArrayList<SlotDto>() {{
                                add(new SlotDto(localDateTime1.toLocalDate(), localDateTime1.toLocalTime(), localDateTime1.toLocalTime().plusHours(1)));
                                add(new SlotDto(localDateTime2.toLocalDate(), localDateTime2.toLocalTime(), localDateTime2.toLocalTime().plusHours(1)));
                            }}
                    )
            );
        }};
        ScheduleInterviewRequest scheduleInterviewRequest = new ScheduleInterviewRequest(new UID(), localDateTime3);
        Response response = (new InterviewPlanning()).scheduleInterview(scheduleInterviewRequest, candidateRepository, recruiterRepository);
        if (response.getData() == null)
            fail(response.getError());
    }

    @Test
    final void scheduleWithOneSlotOccupiedAndManyRecruiterInterviewSucceed() {
        LocalDateTime localDateTime = LocalDateTime.now().plusDays(1);
        ICandidateRepository candidateRepository = uid -> new CandidateDto(uid, "Sylvain", "Miron");
        IRecruiterRepository recruiterRepository = () -> new ArrayList<RecruiterDto>() {{
            add(
                    new RecruiterDto(
                            "Julien",
                            "Brouzes",
                            new ArrayList<SlotDto>() {{
                                add(new SlotDto(localDateTime.toLocalDate(), localDateTime.toLocalTime(), localDateTime.toLocalTime().plusHours(1)));
                            }}
                    )
            );
            add(new RecruiterDto("Ilies", "Lebcir", new ArrayList<>()));
        }};
        ScheduleInterviewRequest scheduleInterviewRequest = new ScheduleInterviewRequest(new UID(), localDateTime);
        Response response = (new InterviewPlanning()).scheduleInterview(scheduleInterviewRequest, candidateRepository, recruiterRepository);
        if (response.getData() == null)
            fail(response.getError());
    }
}
